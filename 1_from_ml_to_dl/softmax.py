"""
Thats the way to get the "probabilities according to scores".

wt scores are? where they come from?
how softmax works?


"""


"""Softmax"""

import numpy as np
import matplotlib.pyplot as plt

def softmax(x):
    return np.exp(x) / np.sum(np.exp(x), axis=0)

# creates an array from -2 to 6 stepping 0.1
x = np.arange(start=-2.0, stop=6.0, step=0.1)
# vstack => puts
# ones_like => copy from of array and fill it with ones
scores = np.vstack([x, np.ones_like(x), 0.2 * np.ones_like(x)])

# wt are this parameters and why?
plt.plot(x, softmax(x).T, linewidth=2)
plt.show()
